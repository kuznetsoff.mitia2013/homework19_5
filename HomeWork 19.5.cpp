﻿#include <iostream>
#include <string>
class Animal {
public:
	virtual void Voice()  const = 0;
};
class Dog : public Animal {

public:
	void Voice()  const override
	{
		std::cout << "WooF\n";
	}

};
class Cat : public Animal {

public:
	void Voice() const  override
	{
		std::cout << "Meow\n";
	}

};
class Wolf : public Animal {

public:
	void Voice() const override
	{
		std::cout << "Yuuuuu\n";
	}

};


int main() {
	Animal* animals[3];
	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Wolf();
	for (Animal* a : animals)
		a->Voice();
}